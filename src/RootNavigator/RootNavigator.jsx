import React from 'react';
import { enableScreens } from 'react-native-screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import News from '../Components/News'
import Home from '../Components/Home'
import Profile from '../Components/Profile'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


enableScreens();
const Stack = createBottomTabNavigator();

const  MyStack = () => {
  return (
      <>
    <Stack.Navigator 
    screenOptions={{
        headerMode: null,
        headerShown:false,
        initialRouteName:"Home"
      }}>
      <Stack.Screen 
        name='Home' 
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Stack.Screen 
        name="News" 
        component={News}
        options={{
          tabBarLabel: 'News',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="newspaper-variant-outline" color={color} size={size} />
          ),
        }}
      />
      <Stack.Screen 
        name="Profile" 
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="face-profile" color={color} size={size} />
          ),
        }}
      />
    </Stack.Navigator>
    </>
  );
}
export default MyStack
import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import {useDispatch} from 'react-redux'
import {logedIn} from '../redux/reducers'
import {getData} from '../api/api'
import { calculatedWidth } from '../styles/size';

const Auth = () => {
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState()
    const [userdata, setUserdata] = useState()
    const [err, setError] = useState()
    const [status, setStatus] = useState()
    const dispatch = useDispatch()
    const logIn = async () => {
        await getData().then(response => {
            setUserdata(response)
        }).catch(e => {
            setError(e)
        })
        userdata.username === login  ? 
            userdata.password === password ? 
                (setStatus('Login successful'), dispatch(logedIn()))
            : 
                setStatus('wrong password') 
        : 
            setStatus('wrong login')

    }

    return (
        <View style={{marginTop:100, marginHorizontal:30}}>
            <Text>{status}{err ? err : null}</Text>
            <TextInput
                style={{width: calculatedWidth(0.8), height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={text => setLogin(text)}
                value={login}
            />
            <TextInput
                style={{width: calculatedWidth(0.8), height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={text => setPassword(text)}
                value={password}
            />
            <Button 
                onPress={() => logIn()}  
                title="logIn"  
            />
        </View>
    )
}

export default Auth
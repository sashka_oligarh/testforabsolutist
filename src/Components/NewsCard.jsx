import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import {calculatedHeight, calculatedWidth} from '../styles/size'
import {handleOpenLink} from '../utils/openLink'
import HTML from 'react-native-render-html';


const NewsCard = (props) => {
    return (
        <View style ={styles.card}>
            <Text style={{marginVertical:5, fontSize:25, fontWeight:'500', color:'#DC143C'}}>
                Author: {props.author}
            </Text>
            <Text style={{marginVertical:5, fontSize:25, fontWeight:'500', color:'#32CD32'}}>
                Title: {props.title}
            </Text>
            <Text style={{marginBottom:20, fontSize:10, fontWeight:'800'}}>
                <HTML html={props.description} />
            </Text>
            {props.urlToImage == null && false ? null : <Image 
                style={styles.img}
                source={{uri:props.urlToImage}} 
            /> 
            }
            <Text><HTML html={props.content} /></Text>
            <Text style={{marginVertical:10,}}>Read full news <Button title={`${props.url}`} onPress={() => handleOpenLink(props.url)} />  </Text>
            <Text>__________________________________________</Text>
        </View>
    )
}

export default NewsCard

const styles = StyleSheet.create({
    card:{
        width:calculatedWidth(0.9),

        margin:20, 
        alignItems:'center',
        resizeMode: 'stretch'
    },
    img:{
        width: calculatedWidth(0.7),
        height:calculatedHeight(0.3),
        margin:20
    }
})

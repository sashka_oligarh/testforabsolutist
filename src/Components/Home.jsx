import { StyleSheet, Text, View, Button  } from 'react-native';
import React from 'react'
import {useSelector} from 'react-redux'
import { useNavigation } from '@react-navigation/native';
import {calculatedHeight} from '../styles/size'





const Home = () => {
    const navigation = useNavigation();
    const isLogin = useSelector(state => state.user.logined)
    
    return (
        <>
            {isLogin ? 
                <View style ={styles.container}>
                    <Text style={{marginBottom:20}}>
                        Check news
                    </Text>
                    <Button
                        title='News'
                        onPress={() => {
                            navigation.navigate('News')
                        }}
                    />
                </View>
            :
                <View style={styles.container} >
                    <Text style={{marginBottom:20}}>
                        Log in to see the tough guy in your profile
                    </Text>
                    <Button
                        title='Login'
                        onPress={() => {
                            navigation.navigate('Profile')
                        }}
                    />
                </View>
            }
        </>
    )
}

export default Home

const styles = StyleSheet.create({
    container:{
        justifyContent:'center', 
        alignItems:'center', 
        height:calculatedHeight(1)
    }
})


import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import Auth from './Auth'
import {calculatedHeight, calculatedWidth} from '../styles/size'
import {useSelector, useDispatch} from 'react-redux'
import {logedOut} from '../redux/reducers'

const Profile = () => {
    const uri = 'https://cs5.pikabu.ru/post_img/2015/12/12/9/144993433914058095.jpg'
    const isLogin = useSelector(state => state.user.logined)
    const dispatch = useDispatch()
    const logOut = () => {
        dispatch(logedOut())
    }
    return (
        <View style={{alignItems:'center', textAlign:'center'}}>
            {isLogin ? 
                <View style={{marginTop:70, alignItems:'center' }}>
                    <Text>Some info about user</Text>
                    <Image 
                        style={styles.img}
                        source={{
                            uri: uri
                            }}
                    />
                    <View style={{flexDirection:'row', justifyContent:'center'}}>
                        <Text style ={{marginRight:10}}>FirstName</Text>
                        <Text>LastName</Text>
                    </View>
                    <Text style={{marginVertical:40}}>Very long description Very long description Very long description Very long description Very long description Very long description Very long description Very long description Very long description Very long description </Text>
                    <Button 
                        onPress={() => logOut()}  
                        title="logOut"  
                    />
                </View> 
            :
                <Auth/>
            }
            
        </View>
    )
}

export default Profile


const styles = StyleSheet.create({
    img:{
        width: calculatedWidth(0.7),
        height:calculatedHeight(0.3),
        margin:20
    }
})

import React, {useEffect} from 'react';
import { StyleSheet, ActivityIndicator, SafeAreaView, FlatList } from 'react-native';
import {newsFetched, newsFetching, newsFetchingError} from '../redux/reducers'
import {useDispatch, useSelector} from 'react-redux'
import {getNews} from '../api/api'
import NewsCard from './NewsCard'
import {calculatedHeight, calculatedWidth} from '../styles/size'
import {setData} from '../api/api'


const News = () => {
    const dispatch = useDispatch()
    const news = useSelector(state => state.news.items)
    const loading = useSelector(state => state.news.loading)
    const lastNews = news
    const user = {
        username: 'Admin',
        password: '12345'
      }
    useEffect(() => {
        setData(user)
        dispatch(newsFetching())
        getNews().then(data => {
            dispatch(newsFetched(data.articles))
        }).catch(e => dispatch(newsFetchingError(e)))
        
    },[])

    const renderItem = ({item}) => (
        <NewsCard
            author={item.author}
            title={item.title}
            description={item.description}
            urlToImage={item.urlToImage}
            content={item.content}
            url={item.url}
        />
    )
    return (
        <>
        {loading ? 
            <ActivityIndicator size={'large'} color={'0000'} style={{marginTop:200}} /> :
        <SafeAreaView style ={styles.container}>
            <FlatList
                data={lastNews}
                renderItem={renderItem}
                keyExtractor={index => index}
            />
        </SafeAreaView>
        }
        </>
    )
}

export default News

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    screen:{
        width:calculatedWidth(0.90),
        height:calculatedHeight(0.9),
        alignItems:'center',
        justifyContent:'center',
        alignItems:'center'
    },
})
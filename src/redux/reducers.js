import { combineReducers, createSlice } from '@reduxjs/toolkit'

const initialState = {
    items:[],
    loading: false,
    failed:false,
    err:null,

}

const news = createSlice({
    name: 'news',
    initialState,
    reducers: {
        newsFetching: (state) => {
                state.loading = true
        },
        newsFetched: (state, action) => {
                state.loading = false,
                state.items = action.payload
        },
        newsFetchingError: (state, action) => {
                state.failed = true,
                state.err = action.payload.err
        }
    }
})

const user = createSlice({
    name:'user',
    initialState: {logined: false},
    reducers: {
        logedIn:(state) => {
            state.logined = true
        },
        logedOut:(state) => {
            state.logined = false
        }
    }
})


export const {
    newsFetched, newsFetching, newsFetchingError
} = news.actions;

export const {
    logedIn, logedOut
} = user.actions;

export const reducer = combineReducers({
    user: user.reducer,
    news: news.reducer
}) 

export default reducer

import axios from 'axios'
import key from '../config/key'
import AsyncStorage from '@react-native-async-storage/async-storage';

const url = 'http://newsapi.org/v2/everything?q=bitcoin&from=2020-12-10&sortBy=publishedAt&apiKey=03398d1c7c5f4cb0a314bb39b1142cce'

export const getNews = async () => {
    console.log('api');
    try { 
        let response = await axios.get(url, {key})
        return response.data

    } catch(e) {
        console.log(e)
    }  
}

export const setData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem('user', jsonValue)
    } catch (e) {
      return e
    }
}

export const getData = async () => {
    try {
       const jsonValue = await AsyncStorage.getItem('user')
       return JSON.parse(jsonValue)
    } catch(e) {
        console.log('error',e)
    }
}
  